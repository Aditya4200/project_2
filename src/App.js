import React from 'react';
import './App.css';
import Price from './component/Price';
// import TakingData from './component/TakingData';


function App() {
  return (
    <div className="App">
      {/* <TakingData/> */}
      <Price/>
    </div>
  );
}

export default App;
