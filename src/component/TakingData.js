import React, { Component } from 'react'
import axios from 'axios'

export class TakingData extends Component {
    constructor(props) {
      super(props)
    
      this.state = {
         persons:['']
        
      }
    }

    componentDidMount=()=>{
        axios.get('https://jsonplaceholder.typicode.com/users')
        .then(res=> {
            console.log(res)
           this.setState({
               persons:res.name,
               
           })
        })
    }
    
  render() {
    return (
      <div>
        <h1>Name List by axios</h1>
        <ul>
            {
                this.state.persons.map(person=>
                <li>{person.name}</li>
                    
                )
            }
        </ul>
      </div>
    )
  }
}

export default TakingData
